all: cv

cv:
	xelatex cv.tex
	xelatex cv.tex

clean: 
	rm *.aux *.bcf *.log *.out *.run.xml
